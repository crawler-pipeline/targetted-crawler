# -*- coding: utf-8 -*-
import scrapy
import os
import urllib
import logging
import re
import random,sys
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from pymongo import MongoClient
from datetime import datetime
#from scrapy.log import ScrapyFileLogObserver
from scrapy import log
from config import env
sys.path.append(env.image_downloader_path)
import downloader
from graingerurl import blinks

class GraingerSpider(scrapy.Spider):
    name = "grainger"
    allowed_domains = ["grainger.com"]
    #start_urls = ["https://www.grainger.com"]

    def __init__(self):
        self.process_id = 1234
        self.db="grainger"
	self.coll="graingerProducts"
	self.local_path=env.relative_path+"/"+self.db
        log_file=str(self.process_id)+"_"+"spider.log"
        error_log_file=str(self.process_id)+"_"+"spider_error.log"
        #ScrapyFileLogObserver(open(log_file, 'w'), level=logging.INFO).start()
        #ScrapyFileLogObserver(open(error_log_file, 'w'), level=logging.ERROR).start()
	dispatcher.connect(self.spider_closed, signal=signals.spider_closed)
    
    def start_requests(self):
        for link in blinks:
             yield scrapy.Request(link, callback=self.parse)

    def spider_closed(self, spider):
	downloader.downup(self.process_id,self.local_path,self.db,self.coll)


    #def parse(self, response):
    #    link1=response.xpath('//div[@class="dropdown-menu"]/ul/li/a/@href').extract()
    #    for links1 in link1:
    #        li1="https://www.grainger.com"+links1
    #        yield scrapy.Request(li1, callback=self.nav_page2)
            
    def parse(self, response):
	#print "\n111111",response.url
        link2=response.xpath('//div[@class="shop-category-row"]/div[@class="shop-category-item"]/a/@href').extract()
	for links2 in link2:
            li2="https://www.grainger.com"+links2
	    yield scrapy.Request(li2, callback=self.nav_page4)   
               
    def nav_page4(self, response):
        #print "\n33333333",response.url
	page_url = response.url
        link4=response.xpath('//div[@class="productImage"]/a/@href').extract()
        #print link4
        for links4 in link4:
            li4="https://www.grainger.com"+links4
	    yield scrapy.Request(li4, callback=self.nav_page5)
        next=response.xpath('//div[@id="content"]/div[@id="body"]/div[@id="main-content"]/div[@id="productSearch"]/section[@class="searchControls"]/div[@class="searchPagination"]/ul/li[@class="next"]/a/@onclick').extract()
        if next:
	    page_url=page_url.split("?")
	    no1=next[0][26]
	    no2=next[0][27]
	    if (no1.isdigit()&no2.isdigit()):
	    	nxt=[no1,no2]
	    	nxt="".join(nxt)
	    else:
	   	nxt=no1
	    if (int(nxt))<=30:
	        url=page_url[0]+"?perPage=32&requestedPage="+nxt
	        yield scrapy.Request(url, callback=self.nav_page4)
	    
        
    def nav_page5(self, response):
	#print "\n444444444",response.url
	client=MongoClient("localhost",27017)
	database_name=self.db
	collection_name=self.coll
	db=client[database_name]
        link5=response.xpath('//div[@class="mainImage"]/img[@id="mainImageZoom"]/@src').extract()
	if (link5 != []):
		link5=link5[0].replace("//static","https://static")
		image_link=link5
		link5=link5.replace("mdmain","zmmain")
		bcrumb=response.xpath('//ul[@class="nav"]/li/a/text()').extract()
		#print link5
		#lvl0=str(bcrumb[0].replace(" ","_"))
		lvl1=str(bcrumb[1]).replace(" ","_")
		lvl2=str(bcrumb[2]).replace(" ","_")
		itemno=response.xpath('//ul/li/span[@itemprop="productID"]/text()').extract()
		price=response.xpath('//span[@class="gcprice-value"]/text()').extract()
		price=price[0].strip()
		name=response.xpath('//ul[@class="nav"]/li/div/text()').extract()
		product_link=response.url
		if (itemno==[]):
			name=name=response.xpath('//div[@class="product-info"]/h1/text()').extract()
			itemno=response.xpath('//div[@class="compare-container"]/div/label/a/text()').extract()
		product_name=str(name[0])
		hierarchy=bcrumb[0]+"/"+bcrumb[1]+"/"+bcrumb[2]
		data={
			"process_id" : int(self.process_id),
			"category1" : lvl1.replace("_"," "),
			"category2" : lvl2.replace("_"," "),
			"product_id" : str(itemno[0]),
			"productlink" : product_link,
			"image_url" : image_link,
			"name" : product_name.strip(),
			"price" : price,
			"source" : "Grainger",
			"hierarchy" : hierarchy
			}
		db[collection_name].insert(data)
	
