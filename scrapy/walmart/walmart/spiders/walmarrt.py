# -*- coding: utf-8 -*-
import scrapy
import os
import urllib
import logging
import re
import random,sys
from scrapy.xlib.pydispatch import dispatcher
from scrapy import signals
from top_cat_link_dict import blinks
from pymongo import MongoClient
from datetime import datetime
#from scrapy.log import ScrapyFileLogObserver
from scrapy import log
from config import env
sys.path.append(env.image_downloader_path)
import downloader

class WalmarrtSpider(scrapy.Spider):
    name = "walmarrt"
    allowed_domains = ["walmart.com"]
    #start_urls = (
    #    'http://www.walmart.com/cp/sports-and-outdoors/4125',
    #)
    def __init__(self, *args, **kwargs):
        self.process_id = kwargs.get('process_id')
        self.allowed_domains = ["walmart.com"]
        self.user_id=kwargs.get('user_id')
        log_file=str(self.process_id)+"_"+"spider.log"
        error_log_file=str(self.process_id)+"_"+"spider_error.log"
        #ScrapyFileLogObserver(open(log_file, 'w'), level=logging.INFO).start()
        #ScrapyFileLogObserver(open(error_log_file, 'w'), level=logging.ERROR).start()
	dispatcher.connect(self.spider_closed, signal=signals.spider_closed)

    def start_requests(self):
        for link in blinks:
            yield scrapy.Request(link, callback=self.parse,meta={'pno': 1})

    def spider_closed(self, spider):
	downloader.downup(self.process_id, env.relative_path,"walmart","walmartCatalogue")

    def parse(self, response):
        #pass class =block-list
	page_no = response.meta['pno']

        bcrumb = response.xpath('//ol[@class="breadcrumb-list"]//text()').extract()
        bb = ""
        for b in bcrumb:
            if len(b.strip())>0:
                bb = bb + b + "/"
        bb = bb.replace('/>/','').replace('//','')
        
        for sel in response.xpath('//a[@class="js-product-image"]'):
            link = sel.xpath('@href').extract()
            if link:
                link = "http://www.walmart.com"+link[0]
                #print link
                yield scrapy.Request(link, callback=self.get_product_image, meta={'bcrumb': bb, 'pno': page_no})
        
        
        next_page = response.xpath('//a[@class="paginator-btn paginator-btn-next"]')
        if next_page:
            base_url=response.url.split("?page")[0]
            next_page = base_url + response.xpath('//a[@class="paginator-btn paginator-btn-next"]/@href').extract()[0]
            #print "NEXT PAGE :: ", next_page

	    matchObj = re.match( r'.*page=(.*?)&.*', next_page, re.M|re.I)
            if matchObj:
                page_no=matchObj.group(1)
            yield scrapy.Request(next_page, callback=self.parse, meta={'pno': page_no})

    def get_product_image(self, response):
	relative_path = "/home/ubuntu/crawled_images"
        client = MongoClient()
        #Connecting to database
        db=client.walmart
        #print response.url
        page_no = response.meta['pno']
	base_url=response.url

	bcrumb = response.meta['bcrumb']
        #print "META bcrumb ", bcrumb
        images = set()
#        print response.xpath('//h1//text()').extract()
        title= response.xpath('//h1//text()').extract()
        new=''.join(title)
        title_product=new.strip()

        for img in response.xpath('//img'):
            if img.xpath('@src'):
                img = img.xpath('@src').extract()[0]
                if "450" in img:
                    images.add(img)
		    image_url = img
			
#url_write variable to write into database
#usc to write product id
#bcrumb to write path into db        
        img2 = response.xpath('//meta[@property="og:image"]/@content')
        if img2:
            images.add(response.xpath('//meta[@property="og:image"]/@content').extract()[0])
				
            image_url=response.xpath('//meta[@property="og:image"]/@content').extract()[0]

        upc = response.xpath('//meta[@itemprop="productID"]/@content')
        if upc:
            upc = response.xpath('//meta[@itemprop="productID"]/@content').extract()[0]
        else:
            upc = str(random.randint(1, 100000))
		 
        path = bcrumb
	
	image_path=path

#For writing data into MONGODB  
        process_id= self.process_id
        user_id= self.user_id
 #For finding level 0 and level 1 from bcrumb
        level=bcrumb.split("/")
        l0=level[0]
        l1=level[1]

        result = db.walmartCatalogue.insert({
                "source"    :"Walmart",
                "url"       : base_url,
                "page_no"   : page_no,
                "product_id": upc,
                "image_url" : image_url,
                "hierarchy" : bcrumb,
                "crawl_date": str(datetime.now()),
                "l0"        : l0,
                "l1"        : l1,
                "title"     :title_product,
                "process_id" : int(process_id),
                "user_id"    : int(user_id)})

        store_location="crawled_process_id_"+process_id+"/"+path







#For downloading images       
		 #if not os.path.isdir(path):
        #    os.makedirs(path)
        
       # for url in images:
        #    location = path + "/" + upc + ".jpg"
         #   resource = urllib.urlopen(url)
          #  output = open("%s"%location,"wb")
           # output.write(resource.read())
            #output.close()
